package database;

import entities.User;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil extends StandardServiceRegistryBuilder {
    protected Configuration config;
    private SessionFactory sessionFactory;


    public Configuration createConfiguration() {
        this.config = new Configuration().configure().addAnnotatedClass(User.class);
        return config;
    }

    public SessionFactory buildSession() {
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(this.config.getProperties()).build();
        this.sessionFactory = config.buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }

}

