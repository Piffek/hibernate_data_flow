package services;

import database.HibernateUtil;
import entities.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class UserService {
    private Session session;
    private SessionFactory sf;

    public UserService() {
        HibernateUtil hibernate = new HibernateUtil();
        hibernate.createConfiguration();
        this.sf = hibernate.buildSession();
        this.session = sf.openSession();
    }

    public void addUser(User user) {

        session.save(user);

    }

    public User getUserById(Integer id) {

        User user = session.get(User.class,id);
        session.close();
        sf.close();

        return user;

    }

}
