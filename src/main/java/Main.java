import entities.User;
import services.UserService;

public class Main {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = new User();
        user.setName("Patryk");
        userService.addUser(user);

        User oneUser = userService.getUserById(1);

        System.out.println(oneUser.getName());

    }
}
